// (c) Raphaël Jakse, 2019
// This file is under the GNU GPLv3 license.

const ouiRE = /^(?:https:)?\/\/[a-z]+\.oui\.sncf\/[\s\S]*$/;
const ouiConnection = /^(?:https:)?\/\/[a-z]+\.oui\.sncf\/espaceclient\//;

browser.webRequest.onBeforeRequest.addListener(
    function (details) {
        if (ouiRE.test(details.documentUrl)) {
            return {cancel: true};
        }
    }, {
        urls: [
            "https://*.oui.sncf/sites/all/themes/hermes/dist/fonts.min.css*",
            "https://*.oui.sncf/sites/all/themes/hermes/dist/fonts.min.css*",
            "https://*.oui.sncf/sites/all/themes/hermes/dist/img/gondola-transparent-bottom--fix.png",
            "https://*.oui.sncf/sites/all/modules/custom_addons/vsct_feature_canvas/theme/dist/img/oui-sncf--white.png",
            "https://*.oui.sncf/sites/default/files/vignette_fond_bleu_0.png",
            "https://*.oui.sncf/*/oui-sncf.svg",
            "https://*.oui.sncf/sites/all/themes/hermes/dist/img/b.gif",
            "https://*.oui.sncf/sites/default/files/imagecache/*",
            "https://*.oui.sncf/sites/all/themes/hermes/*.css*",
            "https://*.oui.sncf/*/sites/all/themes/hermes/dist/fonts.min.css*",
            "https://*.oui.sncf/*/sites/all/themes/hermes/dist/fonts.min.css*",
            "https://*.oui.sncf/*/sites/all/themes/hermes/dist/img/gondola-transparent-bottom--fix.png",
            "https://*.oui.sncf/*/sites/all/modules/custom_addons/vsct_feature_canvas/theme/dist/img/oui-sncf--white.png",
            "https://*.oui.sncf/*/sites/default/files/vignette_fond_bleu_0.png",
            "https://*.oui.sncf/*/sites/all/themes/hermes/dist/img/b.gif",
            "https://*.oui.sncf/*/sites/default/files/*.jpg",
            "https://*.oui.sncf/*/sites/default/files/*.png",
            "https://*.oui.sncf/*/sites/all/themes/hermes/*.css*",
            "https://*.oui.sncf/imgs/*",
            "https://*.oui.sncf/*.woff",
            "https://*.oui.sncf/*.ttf",
            "https://*.oui.sncf/sites/default/files/images/*.jpg",
            "https://en.oui.sncf/media/images/*"
        ]
    },
    ["blocking"]
);

browser.webRequest.onBeforeRequest.addListener(
    function (details) {
        if (ouiRE.test(details.documentUrl)) {
            return {cancel: true};
        }
    }, {
        urls: [
            "*://static.cdn-apple.com/businesschat/*",
        ]
    },
  ["blocking"]
);

browser.webRequest.onBeforeRequest.addListener(
    function (details) {
        if (ouiRE.test(details.documentUrl)) {
            return {cancel: true};
        }
    }, {
    urls: [
//      "https://*.oui.sncf/sites/all/modules/custom_addons/vsct_feature_canvas/theme/dist/canvas-final.min.css*",
        "*://widget.rogervoice.com/*",
        "*://static.doyoudreamup.com/*",
        "https://krum.vsct.fr/rumstats.txt",
        "https://*.abtasty.com/*",
        "https://www.oui.sncf/abtasty/fr/FR.js",
        "https://*.oui.sncf/medias-cdn/csc/prod/offers-catalog*",
        "https://v.oui.sncf/*",
        "https://*.oui.sncf/checkout/front/vendors~oui-feedback.*.js",
//         "https://*.oui.sncf/medias-cdn/cus/PRD/customer-lib-uncached-fuse.js?customerLibCallBack=initCusLibCallback"
        "https://stationfinder.oui.sncf/*",
        "https://cdnjs.cloudflare.com/ajax/libs/leaflet/*",
    ]
  },
  ["blocking"]
);


browser.webRequest.onBeforeRequest.addListener(
    function (details) {
        if (ouiRE.test(details.documentUrl) && !ouiConnection.test(details.documentUrl)) {
            return {cancel: true};
        }
    }, {
    urls: [
//         "https://*.oui.sncf/medias-cdn/ccl/PRD/index-with-deps.1.7.1.js",
//         "https://*.oui.sncf/medias-cdn/cus/PRD/index-with-deps.1.1.0.js",
        "https://*.oui.sncf/sites/all/themes/hermes/dist/new-home-widget-vsa-final.min.js*",
        "https://*.oui.sncf/medias-cdn/csc/prod/csc-widget-react.*.js",
        "https://*.oui.sncf/medias-cdn/csc/prod/csc-uncached-fuse.js?callback=cscWidgetNewHomeCallback",
        "https://*.oui.sncf/medias-cdn/ccl/PRD/authentication-widget-uncached-fuse.js?bootstrapCallbackName=initAuthenticationWidgetCallback",
        "https://wblt.oui.sncf/*",
        "https://*.oui.sncf/sites/all/modules/custom_addons/vsct_feature_canvas/*",
        "https://*.oui.sncf/sites/all/themes/hermes/dist/hermes-final.min.js*",
    ]
  },
  ["blocking"]
);

browser.webRequest.onBeforeRequest.addListener(
    function (details) {
        console.log("details", details);
        return {
            redirectUrl: details.url.replace("&interstitial", "")
        };
    }, {
        urls: [
            "https://*.oui.sncf/*&interstitial"
        ]
    },
    ["blocking"]
);
