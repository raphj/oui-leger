# OUI Léger

(English summary below )

Cette extension Firefox a pour but de rendre le site https://www.oui.sncf/ plus léger (en bande passante et en utilisation des ressources) et rapide en supprimant les ressources lourdes et inutiles du site.
Elle inclut aussi un contournement à l’écran « recherche en cours », qui ne sert qu’à afficher de la publicité aux usagers du site.

ATTENTION : cette extension modifie beaucoup l'apparence de OUI.sncf. Cette extension n’est pas une extension officielle de la SNCF.

L’extension est sous la licence GPLv3 ou plus. Voir le fichier LICENSE pour consulter la licence GPLv3.
Elle pourrait fonctionner sur d’autres navigateurs prenant en charge les WebExtensions, mais n’a pas été testée dessus.

Elle est dans un état expérimental. Elle pourrait casser des fonctionnalités du site.
J’espère qu’elle vous sera utile, mais je ne saurais être tenu pour responsable en cas de mauvais fonctionnement du site à cause de l’extension. Bien sûr, les contributions sont les bienvenues.

En plus de cette extension, je suggère d'activer la protection contre le pistage de Firefox et d'installer l'extension uBlock Origin.

----

This is an addon to lighten the OUI.sncf website (a website to buy tickets for the railways in France), by blocking unnecessary assets. It also removes a delay meant to show ads.
Complementary to this extension, I advise people to enable Firefox anti-tracking proctetion and to install uBlock Origin.

WARNING: this extension is not an official extension from SNCF, and it modifies the look and feel of oui.sncf a lot.
