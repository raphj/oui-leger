// (c) Raphaël Jakse, 2019
// This file is under the GNU GPLv3 license.

function link(l, h) {
    const a = document.createElement("a");
    a.textContent = l;
    a.href = h;
    return a;
}

const p = document.createElement("p");
p.appendChild(link("Accueil", "/"));
p.appendChild(document.createTextNode(" | "));
p.appendChild(link("Espace client", "espaceclient/"));
p.appendChild(document.createTextNode(" | "));
p.appendChild(link("Commandes en cours", "espaceclient/commandes-en-cours/"));
p.appendChild(document.createTextNode(" | "));
p.appendChild(link("Mon voyage", "monvoyage/"));
document.body.appendChild(p);
